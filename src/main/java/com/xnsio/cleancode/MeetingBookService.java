package com.xnsio.cleancode;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

public class MeetingBookService 
{
	private static final String NO_MEETING_CAN_BE_SCHEDULED_BEFORE_8_00_AM_OR_AFTER_5_00_PM = "No Meeting can be scheduled before 8:00 AM or after 5:00 PM";
	private static final String TO_MEET = " to meet";
	private static final String NO_AVAILABLE_TIME_SLOT_FOR = "No available time-slot for ";

	public String avialabelSlot(Map<String, List<CalendarDetails>> calendarsData, List<String> praticipants,LocalDate meetingDate, LocalTime meetingTime) {
		if(meetingTime.getHour() < 8 || meetingTime.getHour() > 17)
			return NO_MEETING_CAN_BE_SCHEDULED_BEFORE_8_00_AM_OR_AFTER_5_00_PM;
		
		Map<String, List<LocalTime>> freeSlot = findAvialableSlot(calendarsData, meetingTime);
		if(!checkCalenderDates(calendarsData, meetingDate) || !calendarsData.keySet().containsAll(praticipants) || freeSlot.isEmpty())
			return String.format("%s%s%s", NO_AVAILABLE_TIME_SLOT_FOR, concatPraticipantsName(praticipants), TO_MEET);
		
		LocalTime commanFreeSlot =	findCommanSlot(freeSlot);
		if(commanFreeSlot == null)
			return String.format("%s%s%s", NO_AVAILABLE_TIME_SLOT_FOR, concatPraticipantsName(praticipants), TO_MEET);
		else 
			return createStringforTime(meetingDate, commanFreeSlot, praticipants);
	}
	
	private String createStringforTime(LocalDate meetingDate, LocalTime commanFreeSlot, List<String> praticipants) {
		String pattern = "hh:mm a";
		String patterndate = "dd";
		
		return String.format("%s %s, %s, is the first available time-slot for %s.", 
					meetingDate.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH), 
					meetingDate.format(DateTimeFormatter.ofPattern(patterndate)), 
					commanFreeSlot.format(DateTimeFormatter.ofPattern(pattern)), 
					concatPraticipantsName(praticipants)); 
	}

	private LocalTime findCommanSlot(Map<String, List<LocalTime>> freeSlotData) {
		List<LocalTime> commanLocalTimes = freeSlotData.values().iterator().next();
		for(Entry<String, List<LocalTime>> entry : freeSlotData.entrySet()) {
			commanLocalTimes.retainAll(entry.getValue());
		}
		if(!commanLocalTimes.isEmpty())
			return commanLocalTimes.get(0);
		return null;
	}

	private Map<String, List<LocalTime>> findAvialableSlot(Map<String, List<CalendarDetails>> calendarsData, LocalTime time) {
		Map<String, List<LocalTime>> freeSlotData = new HashMap<>();
		for(Entry<String, List<CalendarDetails>> entry : calendarsData.entrySet()) {
			for(CalendarDetails day : entry.getValue()) {
				List<LocalTime> freeSlot = day.meetingDataWithFreeSlot(time);
				if(!freeSlot.isEmpty())
					freeSlotData.put(entry.getKey(), freeSlot);
				else
					return new HashMap<>();
			}
		}
		return freeSlotData;
	}

	private boolean checkCalenderDates(Map<String, List<CalendarDetails>> calendarsData, LocalDate date) {
		boolean isDateAvailable = true;
		for(List<CalendarDetails> days : calendarsData.values()) {
			boolean innerFlag = false;
			for(CalendarDetails day : days) {
				if(day.getDate().equals(date))
					innerFlag = true;
			}
			if(!innerFlag)
				isDateAvailable = false;
		}
		return isDateAvailable;
	}

	private String concatPraticipantsName(List<String> praticipants) {
		if(praticipants.isEmpty())
			return "";
		if(praticipants.size() == 1)
			return praticipants.get(0);
		
		StringBuilder nameString = new StringBuilder();
		for(int i = 0; i < praticipants.size(); i++) {
			nameString.append(praticipants.get(i));
			nameString.append((i == praticipants.size() - 1) ? "" : (i == praticipants.size() - 2) ? " and " : ", ");
		}
		return nameString.toString();
	}
}
