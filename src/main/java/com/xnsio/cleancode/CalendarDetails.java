package com.xnsio.cleancode;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalendarDetails {
	LocalDate date; 
	Map<LocalTime, String> meetingData;
	
	public CalendarDetails(LocalDate date, Map<LocalTime, String> meetingData) {
		this.date = date;
		this.meetingData = meetingData;
	}
	
	public LocalDate getDate() {
		return this.date;
	}
	
	public List<LocalTime> meetingDataWithFreeSlot(LocalTime time) {
		List<LocalTime> freeSlotList = new ArrayList<>();
		
		for(Entry<LocalTime, String> entry : meetingData.entrySet()) {
			if(meetingData.get(entry.getKey()).equals("") && entry.getKey().getHour() < time.getHour())
				freeSlotList.add(entry.getKey());
		}
		freeSlotList.sort((o1, o2)-> o1.compareTo(o2));
		return freeSlotList;
	}
}
