package com.xnsio.cleancode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

/**
 * Unit test for Meeting book Service
 */
public class MeetingBookServiceTest 
{
	
	private static final MeetingBookService APP = new MeetingBookService();
	private static final String LUNCH = "Lunch";
	private static final String BUSY = "Busy";
	private static final String FREE = "";
	private static final LocalDate SIX_APRIL = LocalDate.of(2019, 04, 06);
	private static final LocalDate SEVEN_APRIL = LocalDate.of(2019, 04, 07);
	private static final LocalDate TEN_APRIL = LocalDate.of(2019, 04, 10);
	
    public Map<String, List<CalendarDetails>> fillUpData(List<String> praticipants) {
    	Map<String, List<CalendarDetails>> calendarsData = new HashMap<>();
    	List<CalendarDetails> days;
    	
    	if(praticipants.contains("Mini")) {
    		days = new ArrayList<>();
    		days.add(new CalendarDetails(SIX_APRIL,fillUpMeetingData(new String[] {"Meet Brad","Meet Janet","Team Planning",FREE,LUNCH,FREE,BUSY,BUSY,BUSY})));
    		days.add(new CalendarDetails(SEVEN_APRIL,fillUpMeetingData(new String[] {"Meet Janet","Meet Brad",FREE,FREE,LUNCH,FREE,FREE,BUSY,BUSY})));
    		calendarsData.put("Mini", days);
    	}
    	
    	if(praticipants.contains("Bard")) {
    		days = new ArrayList<>();
    		days.add(new CalendarDetails(SIX_APRIL,fillUpMeetingData(new String[] {"Meet Mini","Meet Frank","Team Planning",FREE,LUNCH,"Meet Frank",BUSY,BUSY,BUSY})));
    		days.add(new CalendarDetails(SEVEN_APRIL,fillUpMeetingData(new String[] {"Meet Frank","Meet Mini",FREE,FREE,LUNCH,FREE,FREE,FREE,FREE})));
    		calendarsData.put("Bard", days);
    	}
    	
    	if(praticipants.contains("Janet")) {
    		days = new ArrayList<>();
    		days.add(new CalendarDetails(SIX_APRIL,fillUpMeetingData(new String[] {FREE,"Meet Mini","Team Planning",FREE,LUNCH,BUSY,BUSY,FREE,FREE})));
    		days.add(new CalendarDetails(SEVEN_APRIL,fillUpMeetingData(new String[] {"Meet Mini","Meet Frank",BUSY,FREE,LUNCH,BUSY,FREE,"Meet Frank",FREE})));
    		calendarsData.put("Janet", days);
    	}
    	
    	if(praticipants.contains("Frank")) {
    		days = new ArrayList<>();
    		days.add(new CalendarDetails(SIX_APRIL,fillUpMeetingData(new String[] {FREE,FREE,"Team Planning",LUNCH,BUSY,"Meet Brad",BUSY,BUSY,BUSY})));
    		days.add(new CalendarDetails(SEVEN_APRIL,fillUpMeetingData(new String[] {"Meet Brad","Meet Janet",FREE,LUNCH,FREE,FREE,FREE,"Meet Janet",BUSY})));
    		calendarsData.put("Frank", days);
    	}
    	return calendarsData;
    }
    
    private Map<LocalTime, String> fillUpMeetingData(String[] timeStatus){
    	Map<LocalTime, String> meetingData = new HashMap<>();
    	int count = 8;
    	for(String status : timeStatus)
    		meetingData.put(LocalTime.of(count++, 0), status);
    	return meetingData;
    }
    
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Test
    public void noMeetingCanBeScheduledBefore8andAfter5() {
    	assertEquals("No Meeting can be scheduled before 8:00 AM or after 5:00 PM", APP.avialabelSlot(fillUpData(Arrays.asList("Mini")),
    			Arrays.asList("Mini"), SIX_APRIL, LocalTime.of(7, 0)));
    	assertEquals("No Meeting can be scheduled before 8:00 AM or after 5:00 PM", APP.avialabelSlot(fillUpData(Arrays.asList("Mini")), 
    			Arrays.asList("Mini"), SEVEN_APRIL, LocalTime.of(18, 0)));
    }
    
    @Test
    public void WhenNoCalendarDataForPraticipants() {
    	assertEquals("No available time-slot for Busy and Janet to meet", APP.avialabelSlot(fillUpData(Arrays.asList("Busy", "Janet")), 
    			Arrays.asList("Busy", "Janet"), SIX_APRIL, LocalTime.of(17, 0)));
    }
   
    @Test
    public void WhenNoCalendarDataForGivenDate() {
    	assertEquals("No available time-slot for Mini and Janet to meet", APP.avialabelSlot(fillUpData(Arrays.asList("Mini", "Janet")), 
    			Arrays.asList("Mini", "Janet"), TEN_APRIL, LocalTime.of(9, 0)));
    }
    
    @Test
    public void miniWantsToCheckOwnFreeSlotAt6AprilBefore10PM() {
    	assertEquals("No available time-slot for Mini to meet", new MeetingBookService().avialabelSlot(fillUpData(Arrays.asList("Mini")),
    			Arrays.asList("Mini"), SIX_APRIL, LocalTime.of(11, 0)));
    }
    
    @Test
    public void miniWantsToCheckFreeSlotAt6AprilBefore5PMwithFrank() {
    	assertEquals("Sunday 07, 10:00 AM, is the first available time-slot for Mini and Frank.", new MeetingBookService().avialabelSlot(fillUpData(Arrays.asList("Mini", "Frank")),
    			Arrays.asList("Mini", "Frank"), SEVEN_APRIL, LocalTime.of(16, 0)));
    }
    
    @Test
    public void frankWantsToCheckFreeSlotAt7AprilBefore5PMwithJanetAndBard() {
    	assertEquals("Sunday 07, 02:00 PM, is the first available time-slot for Frank, Janet and Bard.", new MeetingBookService().avialabelSlot(fillUpData(Arrays.asList("Frank", "Janet", "Bard")),
    			Arrays.asList("Frank", "Janet", "Bard"), SEVEN_APRIL, LocalTime.of(16, 0)));
    }
}
